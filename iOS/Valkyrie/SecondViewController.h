//
//  SecondViewController.h
//  Valkyrie
//
//  Created by PETAR LAZAROV on 4/6/15.
//  Copyright (c) 2015 Rapid Development Crew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RNGridMenu.h"
#import <SpeechKit/SpeechKit.h>
#import "AppDelegate.h"


@interface SecondViewController : UIViewController <RNGridMenuDelegate, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, SpeechKitDelegate, SKRecognizerDelegate>

@property (strong, nonatomic) SKRecognizer* voiceSearch;

@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak, nonatomic) IBOutlet UIButton *recordButton;
@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) SKVocalizer* vocalizer;
@property BOOL isSpeaking;

- (IBAction)recordButtonTapped:(id)sender;

@end

