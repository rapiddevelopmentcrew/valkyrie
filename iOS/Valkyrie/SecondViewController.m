//
//  SecondViewController.m
//  Valkyrie
//
//  Created by PETAR LAZAROV on 4/6/15.
//  Copyright (c) 2015 Rapid Development Crew. All rights reserved.
//

#import "SecondViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "RNGridMenu.h"
#import "ThirdViewController.h"

@interface SecondViewController ()
@property (weak, nonatomic) IBOutlet UILabel *tempSensor;
@property (weak, nonatomic) IBOutlet UILabel *hPaSensor;
@property (weak, nonatomic) IBOutlet UILabel *humiditySensor;
@property (weak, nonatomic) IBOutlet UILabel *lumSensor;
@property (weak, nonatomic) IBOutlet UILabel *hearthRateSensor;
@property (weak, nonatomic) IBOutlet UILabel *stepsSensor;
//@property (weak, nonatomic) IBOutlet UILabel *textFromMic;
@property (nonatomic, strong) RNGridMenu *rngridmenu;
@property (weak, nonatomic) IBOutlet UILabel *labelRec;
@property NSString* resultString;

@end

const unsigned char SpeechKitApplicationKey[] = {0xc5, 0xd9, 0x3e, 0xc9, 0xb2, 0x9b, 0xec, 0x69, 0xcf, 0x5e, 0x1d, 0x18, 0x80, 0x84, 0x9d, 0x0e, 0x17, 0x31, 0x83, 0x33, 0x10, 0x10, 0x9a, 0xfe, 0x1c, 0x9d, 0x8d, 0xff, 0xa8, 0xb6, 0x5d, 0x6d, 0x90, 0x9b, 0x36, 0x42, 0x37, 0x22, 0xac, 0x06, 0x81, 0xc9, 0x1e, 0x01, 0x4d, 0xbc, 0x0f, 0x65, 0xed, 0x93, 0x98, 0xfe, 0xa3, 0x32, 0xfc, 0xbf, 0xcf, 0x89, 0xbf, 0x77, 0x40, 0x1c, 0xe4, 0xd0};

@implementation SecondViewController
NSMutableURLRequest *request;
NSInteger tempCheck;
NSInteger humCheck;
NSInteger stepsF;
NSInteger hearthRF;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadData];
    [self voiceR];
    [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(myTimerTick:) userInfo:nil repeats:YES];
}

- (void) voiceR {
    
    self.appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [self.appDelegate setupSpeechKitConnection];
    
    self.searchTextField.returnKeyType = UIReturnKeySearch;
}
- (IBAction)recordButtonTapped:(id)sender {
    self.recordButton.selected = !self.recordButton.isSelected;
    
    // This will initialize a new speech recognizer instance
    if (self.recordButton.isSelected) {
        self.voiceSearch = [[SKRecognizer alloc] initWithType:SKSearchRecognizerType
                                                    detection:SKShortEndOfSpeechDetection
                                                     language:@"en_US"
                                                     delegate:self];
    }
    
    // This will stop existing speech recognizer processes
    else {
        if (self.voiceSearch) {
            [self.voiceSearch stopRecording];
            [self.voiceSearch cancel];
        }
    }
}

- (void)recognizer:(SKRecognizer *)recognizer didFinishWithResults:(SKRecognition *)results {
    long numOfResults = [results.results count];
    
    if (numOfResults > 0) {
        self.labelRec.text = [results firstResult];
        
        self.resultString = self.searchTextField.text;
        
        
        if ([self.labelRec.text isEqualToString:@"State one"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?mode=0"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"State two"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?mode=1"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"State three"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?mode=2"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"State four"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?mode=3"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Grab one"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?grab=0"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Grab two"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?grab=1"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Grab three"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?grab=2"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Grab four"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?grab=3"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Light on"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?light=1"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Lights on"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?light=1"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Turn the light on"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?light=1"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Turn the lights on"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?light=1"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Light off"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?light=0"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Lights off"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?light=0"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Turn the light off"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?light=0"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Turn the lights off"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?light=0"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Heat on"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?heater=1"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Heater on"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?heater=1"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Turn the heat on"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?heater=1"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Turn the heater on"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?heater=1"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Heat off"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?heater=0"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Heater off"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?heater=0"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Turn the heat off"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?heater=0"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Turn the heater off"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?heater=0"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Switch one on"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?turnon=0"]];
            NSLog(@"Switch one");
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Switch one off"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?turnoff=0"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Switch two on"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?turnon=1"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Switch two off"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?turnoff=1"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Switch three on"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?turnon=2"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Switch three off"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?turnoff=2"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Switch four on"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?turnon=3"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Switch four off"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?turnoff=3"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Switch five on"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?turnon=4"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Switch five off"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?turnoff=4"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Switch six on"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?turnon=5"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Switch six off"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?turnoff=5"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Switch seven on"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?turnon=6"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Switch seven off"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?turnoff=6"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Switch eight on"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?turnon=7"]];
            [self postRequest];
        }
        if ([self.labelRec.text isEqualToString:@"Switch eight off"]) {
            request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?turnoff=7"]];
            [self postRequest];
        }
    }
    
    self.recordButton.selected = !self.recordButton.isSelected;
    
    if (self.voiceSearch) {
        [self.voiceSearch cancel];
    }
}

-(void) loadData {
    NSString *post = [NSString stringWithFormat:@""];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?"]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    NSURLResponse *requestResponse;
    NSData *requestHandler = [NSURLConnection sendSynchronousRequest:request returningResponse:&requestResponse error:nil];
    
    NSString *requestReply = [[NSString alloc] initWithBytes:[requestHandler bytes] length:[requestHandler length] encoding:NSASCIIStringEncoding];
    NSLog(@"requestReply: %@", requestReply);
    
    NSArray *myArray = [requestReply componentsSeparatedByString:@"\n"];
    [myArray objectAtIndex:0];
    [myArray objectAtIndex:1];
    [myArray objectAtIndex:2];
    [myArray objectAtIndex:3];
    [myArray objectAtIndex:4];
    [myArray objectAtIndex:5];
    [myArray objectAtIndex:6];
    
    tempCheck = [myArray[0] integerValue];
    
    if (tempCheck > 1 ) {
        NSString *tempS = [NSString stringWithFormat:@"%@ C",myArray[0]];
        self.tempSensor.text = tempS;
    } else {
        NSString *tempS = [NSString stringWithFormat:@"22 C"];
        self.tempSensor.text = tempS;
    }
    
    humCheck = [myArray[3] integerValue];
    
    if (tempCheck > 1 ) {
        NSString *humS = [NSString stringWithFormat:@"%@ RH",myArray[3]];
        self.humiditySensor.text = humS;
    } else {
        NSString *humS = [NSString stringWithFormat:@"33%% RH"];
        self.humiditySensor.text = humS;
    }
    
    hearthRF = [myArray[5] integerValue];
    stepsF = [myArray[6] integerValue];
    
    NSString *hPaS = [NSString stringWithFormat:@"%@ hPa",myArray[2]];
    self.hPaSensor.text = hPaS;
    NSString *lumS = [NSString stringWithFormat:@"%@ lum",myArray[4]];
    self.lumSensor.text = lumS;
    NSString *hrS = [NSString stringWithFormat:@"%ld BPM",(long)hearthRF];
    self.hearthRateSensor.text = hrS;
    NSString *stS = [NSString stringWithFormat:@"%ld Steps",(long)stepsF];
    self.stepsSensor.text = stS;
}


- (void) postRequest {
    
    NSString *post = [NSString stringWithFormat:@""];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    NSURLResponse *requestResponse;
    NSData *requestHandler = [NSURLConnection sendSynchronousRequest:request returningResponse:&requestResponse error:nil];
    
    NSString *requestReply = [[NSString alloc] initWithBytes:[requestHandler bytes] length:[requestHandler length] encoding:NSASCIIStringEncoding];
    NSLog(@"requestReply: %@", requestReply);
    
    NSArray *myArray = [requestReply componentsSeparatedByString:@"\n"];
    [myArray objectAtIndex:0];
    [myArray objectAtIndex:1];
    [myArray objectAtIndex:2];
    [myArray objectAtIndex:3];
    [myArray objectAtIndex:4];
    [myArray objectAtIndex:5];
    [myArray objectAtIndex:6];
    
    tempCheck = [myArray[0] integerValue];
    
    if (tempCheck > 1 ) {
        NSString *tempS = [NSString stringWithFormat:@"%@ C",myArray[0]];
        self.tempSensor.text = tempS;
    } else {
        NSString *tempS = [NSString stringWithFormat:@"22 C"];
        self.tempSensor.text = tempS;
    }
    
    humCheck = [myArray[3] integerValue];
    
    if (tempCheck > 1 ) {
        NSString *humS = [NSString stringWithFormat:@"%@ RH",myArray[3]];
        self.humiditySensor.text = humS;
    } else {
        NSString *humS = [NSString stringWithFormat:@"33%% RH"];
        self.humiditySensor.text = humS;
    }
    
    NSString *hPaS = [NSString stringWithFormat:@"%@ hPa",myArray[2]];
    self.hPaSensor.text = hPaS;
    NSString *lumS = [NSString stringWithFormat:@"%@ lum",myArray[4]];
    self.lumSensor.text = lumS;
    NSString *hrS = [NSString stringWithFormat:@"%ld BPM",(long)hearthRF];
    self.hearthRateSensor.text = hrS;
    NSString *stS = [NSString stringWithFormat:@"%ld Steps",(long)stepsF];
    self.stepsSensor.text = stS;
}

-(void)myTimerTick:(NSTimer *)timer
{   request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?"]];
    [self postRequest];
    NSLog(@"UPDATE");
}

- (IBAction)hearthRateDown:(id)sender {
    hearthRF--;
    NSLog(@"H-");
}

- (IBAction)hearthRateUp:(id)sender {
    hearthRF++;
    NSLog(@"H+");
}

- (IBAction)stepsUp:(id)sender {
    stepsF++;
    NSLog(@"S+");
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
