//
//  ThirdViewController.h
//  Valkyrie
//
//  Created by PETAR LAZAROV on 4/6/15.
//  Copyright (c) 2015 Rapid Development Crew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RNGridMenu.h"

@interface ThirdViewController : UIViewController<RNGridMenuDelegate>
{
 IBOutlet UILabel* temperaturelabel;
 IBOutlet UISwitch *switch1Control;
 IBOutlet UISwitch *switch2Control;
 IBOutlet UISwitch *switch3Control;
 IBOutlet UISwitch *switch4Control;
 IBOutlet UISwitch *switch5Control;
 IBOutlet UISwitch *switch6Control;
 IBOutlet UISwitch *switch7Control;
 IBOutlet UISwitch *switch8Control;
}

@property (weak, nonatomic) IBOutlet UISegmentedControl *modeControl1;
@property NSString* postURL;
@property (strong, nonatomic) IBOutlet UISegmentedControl* modeControl;
//@property (weak, nonatomic) IBOutlet UISegmentedControl *celsiumFahrenheitt;
@property NSInteger temperature;
@property (weak, nonatomic) IBOutlet UISwitch *heatControl;
@property (weak, nonatomic) IBOutlet UISwitch *lightControl;
@property (weak, nonatomic) IBOutlet UISwitch *SetTempControl;






@end
