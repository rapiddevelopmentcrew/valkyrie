//
//  FirstViewController.h
//  Valkyrie
//
//  Created by PETAR LAZAROV on 4/6/15.
//  Copyright (c) 2015 Rapid Development Crew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstViewController : UIViewController{
    IBOutlet UILabel *label;
    IBOutlet UILabel *label1;
    IBOutlet UILabel *label2;
    IBOutlet UILabel *label3;
    
    NSTimer *timer;
}
-(void)updateTimer;
@property (strong, nonatomic) IBOutlet UIImageView *im1;
@property (strong, nonatomic) IBOutlet UIImageView *im2;

@end

