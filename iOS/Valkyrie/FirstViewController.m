//
//  FirstViewController.m
//  Valkyrie
//
//  Created by PETAR LAZAROV on 4/6/15.
//  Copyright (c) 2015 Rapid Development Crew. All rights reserved.
//

#import "FirstViewController.h"

@interface FirstViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *stepsImageView;
@property (weak, nonatomic) IBOutlet UIImageView *hearthImageView;
@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    timer = [NSTimer scheduledTimerWithTimeInterval:0.5
                                             target:self
                                           selector:@selector(updateTimer)
                                           userInfo:nil 
                                            repeats:YES];
}
-(void)updateTimer {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"hh:mm"];
    label.text = [formatter stringFromDate:[NSDate date]];
    label1.text = [formatter stringFromDate:[NSDate date]];
    label2.text = [formatter stringFromDate:[NSDate date]];
    label3.text = [formatter stringFromDate:[NSDate date]];

}
- (IBAction)image1:(id)sender {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
