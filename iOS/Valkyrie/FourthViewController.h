//
//  FourthViewController.h
//  Valkyrie
//
//  Created by PETAR LAZAROV on 4/10/15.
//  Copyright (c) 2015 Rapid Development Crew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RNGridMenu.h"

@interface FourthViewController : UIViewController


@property (weak, nonatomic) IBOutlet UISegmentedControl *segLightOn;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segLightOff;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segHeaterOn;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segHeaterOff;







@end
