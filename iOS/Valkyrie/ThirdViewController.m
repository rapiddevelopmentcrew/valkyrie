//
//  ThirdViewController.m
//  Valkyrie
//
//  Created by PETAR LAZAROV on 4/6/15.
//  Copyright (c) 2015 Rapid Development Crew. All rights reserved.
//

#import "ThirdViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "RNGridMenu.h"

@interface ThirdViewController ()
@property (nonatomic, strong) RNGridMenu *rngridmenu;
@end

@implementation ThirdViewController
NSInteger temp=25;

NSMutableURLRequest *request;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.temperature = 25;
    self.modeControl1.hidden = YES;
}

- (IBAction)SetTempSwitch:(id)sender {
    if (self.SetTempControl.on) {
        request = [[NSMutableURLRequest alloc] init];
        NSString *urlAdress = [NSString stringWithFormat:@"https://agent.electricimp.com/K5cBLKohWns7?temp=%ld", temp];
        [request setURL:[NSURL URLWithString:urlAdress]];
        [self postRequest];
        NSLog(@"Heater request:%@", request);

    }
}

- (IBAction)switch1Operator:(id)sender {
    if (switch1Control.on) {
        request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?turnon=0"]];
        [self postRequest];
    }
    else {
        request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?turnoff=0"]];
        [self postRequest];
    }
}
- (IBAction)switch2Operator:(id)sender {
    if (switch2Control.on) {
        request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?turnon=1"]];
        [self postRequest];
    } else {
        request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?turnoff=1"]];
        [self postRequest];
    }
}
- (IBAction)switch3Operator:(id)sender {
    if (switch3Control.on) {
        request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?turnon=2"]];
        [self postRequest];
    } else {
        request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?turnoff=2"]];
        [self postRequest];
    }
}
- (IBAction)switch4Operator:(id)sender {
    if (switch4Control.on) {
        request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?turnon=3"]];
        [self postRequest];
    } else {
        request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?turnoff=3"]];
        [self postRequest];
    }
}
- (IBAction)switch5Operator:(id)sender {
    if (switch5Control.on) {
        request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?turnon=4"]];
        [self postRequest];
    } else {
        request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?turnoff=4"]];
        [self postRequest];
    }
}
- (IBAction)switch6Operator:(id)sender {
    if (switch6Control.on) {
        request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?turnon=5"]];
        [self postRequest];
    } else {
        request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?turnoff=5"]];
        [self postRequest];
    }
}
- (IBAction)switch7Operator:(id)sender {
    if (switch7Control.on) {
        request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?turnon=6"]];
        [self postRequest];
    } else {
        request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?turnoff=6"]];
        [self postRequest];
    }
}
- (IBAction)switch8Operator:(id)sender {
    if (switch8Control.on) {
        request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?turnon=7"]];
        [self postRequest];
    } else {
        request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?turnoff=7"]];
        [self postRequest];
    }
}

- (IBAction)heatOperator:(id)sender {
    if (self.heatControl.on) {
        request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?heater=1"]];
        [self postRequest];
    } else {
        request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?heater=0"]];
        [self postRequest];
    }
}

- (IBAction)lightOperator:(id)sender {
    if (self.lightControl.on) {
        request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?light=1"]];
        [self postRequest];
    } else {
        request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?light=0"]];
        [self postRequest];
    }
}

- (IBAction)modeChange1:(id)sender {
    if (self.modeControl1.selectedSegmentIndex == 0) {
        request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?grab=0"]];
        [self postRequest];
    }
    if (self.modeControl1.selectedSegmentIndex == 1) {
        request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?grab=3"]];
        [self postRequest];
    }
    if (self.modeControl1.selectedSegmentIndex == 2) {
        request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?grab=1"]];
        [self postRequest];
    }
    if (self.modeControl1.selectedSegmentIndex == 3) {
        request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?grab=2"]];
        [self postRequest];
    }
}

- (IBAction)modeChange:(id)sender {
    if (self.modeControl.selectedSegmentIndex == 0) {
        request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?mode=0"]];
        [self postRequest];
        self.modeControl1.hidden = YES;
    }
    if (self.modeControl.selectedSegmentIndex == 1) {
        request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?mode=1"]];
        [self postRequest];
        self.modeControl1.hidden = YES;
    }
    if (self.modeControl.selectedSegmentIndex == 2) {
        request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?mode=2"]];
        [self postRequest];
        self.modeControl1.selectedSegmentIndex = 0;
        self.modeControl1.hidden = NO;
    }
    if (self.modeControl.selectedSegmentIndex == 3) {
        request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://agent.electricimp.com/K5cBLKohWns7?mode=3"]];
        [self postRequest];
        self.modeControl1.hidden = YES;
    }
}

- (IBAction)sliderTemp:(UISlider*)sender {
        self.temperature = sender.value;
        temperaturelabel.text = [NSString stringWithFormat:@"%.0f C", sender.value];
    temp = sender.value;
    [self SetTempSwitch:sender];
}

- (void) postRequest {
    NSString *post = [NSString stringWithFormat:@""];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];

    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    NSURLResponse *requestResponse;
    NSData *requestHandler = [NSURLConnection sendSynchronousRequest:request returningResponse:&requestResponse error:nil];
    
    NSString *requestReply = [[NSString alloc] initWithBytes:[requestHandler bytes] length:[requestHandler length] encoding:NSASCIIStringEncoding];
    NSLog(@"requestReply: %@", requestReply);
    NSLog(@"");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
