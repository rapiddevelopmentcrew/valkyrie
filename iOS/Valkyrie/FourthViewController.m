//
//  FourthViewController.m
//  Valkyrie
//
//  Created by PETAR LAZAROV on 4/10/15.
//  Copyright (c) 2015 Rapid Development Crew. All rights reserved.
//

#import "FourthViewController.h"

@interface FourthViewController ()

@end


@implementation FourthViewController

NSArray *arr;

- (void) f {
    if (self.segLightOn.selectedSegmentIndex == self.segLightOff.selectedSegmentIndex) {
        self.segLightOff.selectedSegmentIndex = 0;
    } else {
    if (self.segLightOn.selectedSegmentIndex == self.segHeaterOn.selectedSegmentIndex) {
        self.segHeaterOn.selectedSegmentIndex = 0;
    } else {
    if (self.segLightOn.selectedSegmentIndex == self.segHeaterOff.selectedSegmentIndex) {
        self.segHeaterOff.selectedSegmentIndex = 0;
    }else {
            }
        }
    }
}
-(void) f1 {
    if (self.segLightOff.selectedSegmentIndex == self.segLightOn.selectedSegmentIndex) {
        self.segLightOn.selectedSegmentIndex = 0;
    } else {
    if (self.segLightOff.selectedSegmentIndex == self.segHeaterOn.selectedSegmentIndex) {
        self.segHeaterOn.selectedSegmentIndex = 0;
    } else {
    if (self.segLightOff.selectedSegmentIndex == self.segHeaterOff.selectedSegmentIndex) {
        self.segHeaterOff.selectedSegmentIndex = 0;
    } else {
            }
        }
    }
}
-(void) f2 {
    if (self.segHeaterOn.selectedSegmentIndex == self.segLightOn.selectedSegmentIndex) {
        self.segLightOn.selectedSegmentIndex = 0;
    }else {
    if (self.segHeaterOn.selectedSegmentIndex == self.segLightOff.selectedSegmentIndex) {
        self.segLightOff.selectedSegmentIndex = 0;
    }else {
    if (self.segHeaterOn.selectedSegmentIndex == self.segHeaterOff.selectedSegmentIndex) {
        self.segHeaterOff.selectedSegmentIndex = 0;
    }else {
            }
        }
    }
}
-(void) f3 {
    if (self.segHeaterOff.selectedSegmentIndex == self.segLightOn.selectedSegmentIndex) {
        self.segLightOn.selectedSegmentIndex = 0;
    }else {
    if (self.segHeaterOff.selectedSegmentIndex == self.segLightOff.selectedSegmentIndex) {
        self.segLightOff.selectedSegmentIndex = 0;
    }else {
    if (self.segHeaterOff.selectedSegmentIndex == self.segHeaterOn.selectedSegmentIndex) {
        self.segHeaterOn.selectedSegmentIndex = 0;
    }else{
            }
        }
    }
}
- (IBAction)segmentLightsOn:(id)sender {
    if (self.segLightOn.selectedSegmentIndex == 0 ) {
    }
    if (self.segLightOn.selectedSegmentIndex == 1 ) {
        [self f];
    }
    if (self.segLightOn.selectedSegmentIndex == 2 ) {
        [self f];
    }
    if (self.segLightOn.selectedSegmentIndex == 3 ) {
        [self f];
    }
    if (self.segLightOn.selectedSegmentIndex == 4 ) {
        [self f];
    }
    if (self.segLightOn.selectedSegmentIndex == 5 ) {
        [self f];
    }
    if (self.segLightOn.selectedSegmentIndex == 6 ) {
        [self f];
    }
    if (self.segLightOn.selectedSegmentIndex == 7 ) {
        [self f];
    }
    if (self.segLightOn.selectedSegmentIndex == 8 ) {
        [self f];
    }
    if (self.segLightOn.selectedSegmentIndex == 9 ) {
        [self f];
    }
    if (self.segLightOn.selectedSegmentIndex == 10 ) {
        [self f];
    }
    if (self.segLightOn.selectedSegmentIndex == 11 ) {
        [self f];
    }
    if (self.segLightOn.selectedSegmentIndex == 12 ) {
        [self f];
    }
    if (self.segLightOn.selectedSegmentIndex == 13 ) {
        [self f];
    }
    if (self.segLightOn.selectedSegmentIndex == 14 ) {
        [self f];
    }
    if (self.segLightOn.selectedSegmentIndex == 15 ) {
        [self f];
    }
    if (self.segLightOn.selectedSegmentIndex == 16 ) {
        [self f];
    }
}
- (IBAction)segmentLightOff:(id)sender {
    if (self.segLightOff.selectedSegmentIndex == 0 ) {
    }
    if (self.segLightOff.selectedSegmentIndex == 1 ) {
        [self f1];
    }
    if (self.segLightOff.selectedSegmentIndex == 2 ) {
        [self f1];
    }
    if (self.segLightOff.selectedSegmentIndex == 3 ) {
        [self f1];
    }
    if (self.segLightOff.selectedSegmentIndex == 4 ) {
        [self f1];
    }
    if (self.segLightOff.selectedSegmentIndex == 5 ) {
        [self f1];
    }
    if (self.segLightOff.selectedSegmentIndex == 6 ) {
        [self f1];
    }
    if (self.segLightOff.selectedSegmentIndex == 7 ) {
        [self f1];
    }
    if (self.segLightOff.selectedSegmentIndex == 8 ) {
        [self f1];
    }
    if (self.segLightOff.selectedSegmentIndex == 9 ) {
        [self f1];
    }
    if (self.segLightOff.selectedSegmentIndex == 10 ) {
        [self f1];
    }
    if (self.segLightOff.selectedSegmentIndex == 11 ) {
        [self f1];
    }
    if (self.segLightOff.selectedSegmentIndex == 12 ) {
        [self f1];
    }
    if (self.segLightOff.selectedSegmentIndex == 13 ) {
        [self f1];
    }
    if (self.segLightOff.selectedSegmentIndex == 14 ) {
        [self f1];
    }
    if (self.segLightOff.selectedSegmentIndex == 15 ) {
        [self f1];
    }
    if (self.segLightOff.selectedSegmentIndex == 16 ) {
        [self f1];
    }
}
- (IBAction)segmentHeaterOn:(id)sender {
    if (self.segHeaterOn.selectedSegmentIndex == 0 ) {
    }
    if (self.segHeaterOn.selectedSegmentIndex == 1 ) {
        [self f2];
    }
    if (self.segHeaterOn.selectedSegmentIndex == 2 ) {
        [self f2];
    }
    if (self.segHeaterOn.selectedSegmentIndex == 3 ) {
        [self f2];
    }
    if (self.segHeaterOn.selectedSegmentIndex == 4 ) {
        [self f2];
    }
    if (self.segHeaterOn.selectedSegmentIndex == 5 ) {
        [self f2];
    }
    if (self.segHeaterOn.selectedSegmentIndex == 6 ) {
        [self f2];
    }
    if (self.segHeaterOn.selectedSegmentIndex == 7 ) {
        [self f2];
    }
    if (self.segHeaterOn.selectedSegmentIndex == 8 ) {
        [self f2];
    }
    if (self.segHeaterOn.selectedSegmentIndex == 9 ) {
        [self f2];
    }
    if (self.segHeaterOn.selectedSegmentIndex == 10 ) {
        [self f2];
    }
    if (self.segHeaterOn.selectedSegmentIndex == 11 ) {
        [self f2];
    }
    if (self.segHeaterOn.selectedSegmentIndex == 12 ) {
        [self f2];
    }
    if (self.segHeaterOn.selectedSegmentIndex == 13 ) {
        [self f2];
    }
    if (self.segHeaterOn.selectedSegmentIndex == 14 ) {
        [self f2];
    }
    if (self.segHeaterOn.selectedSegmentIndex == 15 ) {
        [self f2];
    }
    if (self.segHeaterOn.selectedSegmentIndex == 16 ) {
        [self f2];
    }
}
- (IBAction)segmentHeaterOff:(id)sender {
    if (self.segHeaterOff.selectedSegmentIndex == 0 ) {
    }
    if (self.segHeaterOff.selectedSegmentIndex == 1 ) {
        [self f3];
    }
    if (self.segHeaterOff.selectedSegmentIndex == 2 ) {
        [self f3];
    }
    if (self.segHeaterOff.selectedSegmentIndex == 3 ) {
        [self f3];
    }
    if (self.segHeaterOff.selectedSegmentIndex == 4 ) {
        [self f3];
    }
    if (self.segHeaterOff.selectedSegmentIndex == 5 ) {
        [self f3];
    }
    if (self.segHeaterOff.selectedSegmentIndex == 6 ) {
        [self f3];
    }
    if (self.segHeaterOff.selectedSegmentIndex == 7 ) {
        [self f3];
    }
    if (self.segHeaterOff.selectedSegmentIndex == 8 ) {
        [self f3];
    }
    if (self.segHeaterOff.selectedSegmentIndex == 9 ) {
        [self f3];
    }
    if (self.segHeaterOff.selectedSegmentIndex == 10 ) {
        [self f3];
    }
    if (self.segHeaterOff.selectedSegmentIndex == 11 ) {
        [self f3];
    }
    if (self.segHeaterOff.selectedSegmentIndex == 12 ) {
        [self f3];
    }
    if (self.segHeaterOff.selectedSegmentIndex == 13 ) {
        [self f3];
    }
    if (self.segHeaterOff.selectedSegmentIndex == 14 ) {
        [self f3];
    }
    if (self.segHeaterOff.selectedSegmentIndex == 15 ) {
        [self f3];
    }
    if (self.segHeaterOff.selectedSegmentIndex == 16 ) {
        [self f3];
    }
}

- (void)bi {
    NSInteger numberOfOptions = 1;
    RNGridMenu *av = [[RNGridMenu alloc] initWithItems:[arr subarrayWithRange:NSMakeRange(0, numberOfOptions)]];
    av.delegate = self;
    [av showInViewController:self center:CGPointMake(self.view.bounds.size.width/2.f, self.view.bounds.size.height/2.f)]; }
- (IBAction)b1:(id)sender {
    arr =@[[[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"s1.png"] title:@""]];    [self bi];  }
- (IBAction)b2:(id)sender {
    arr =@[[[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"s2.png"] title:@""]];    [self bi];  }
- (IBAction)b3:(id)sender {
    arr =@[[[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"s3.png"] title:@""]];    [self bi];  }
- (IBAction)b4:(id)sender {
    arr =@[[[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"s4.png"] title:@""]];    [self bi];  }
- (IBAction)b5:(id)sender {
    arr =@[[[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"s5.png"] title:@""]];    [self bi];  }
- (IBAction)b6:(id)sender {
    arr =@[[[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"s6.png"] title:@""]];    [self bi];  }
- (IBAction)b7:(id)sender {
    arr =@[[[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"s7.png"] title:@""]];    [self bi];  }
- (IBAction)b8:(id)sender {
    arr =@[[[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"s8.png"] title:@""]];    [self bi];  }
- (IBAction)b9:(id)sender {
    arr =@[[[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"s9.png"] title:@""]];    [self bi];  }
- (IBAction)b10:(id)sender {
    arr =@[[[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"s10.png"] title:@""]];    [self bi]; }
- (IBAction)b11:(id)sender {
    arr =@[[[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"s11.png"] title:@""]];    [self bi]; }
- (IBAction)b12:(id)sender {
    arr =@[[[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"s12.png"] title:@""]];    [self bi]; }
- (IBAction)b13:(id)sender {
    arr =@[[[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"s13.png"] title:@""]];    [self bi]; }
- (IBAction)b14:(id)sender {
    arr =@[[[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"s14.png"] title:@""]];    [self bi]; }
- (IBAction)b15:(id)sender {
    arr =@[[[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"s15.png"] title:@""]];    [self bi]; }
- (IBAction)b16:(id)sender {
    arr =@[[[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"s16.png"] title:@""]];    [self bi]; }

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
