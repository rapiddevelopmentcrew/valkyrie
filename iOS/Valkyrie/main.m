//
//  main.m
//  Valkyrie
//
//  Created by PETAR LAZAROV on 4/12/15.
//  Copyright (c) 2015 Rapid Development Crew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
