server.log("Set temp: " + http.agenturl());
local responseString = "12.5\n24.8\n1011.4\n55\n81762\n"+"84\n" + "1982\n";
//responseString[] = "12.5\n24.8\n1011.4\n55\n81762\n";
//ocal responseString = "https://agent.electricimp.com/K5cBLKohWns7?temp=25\nhttps://agent.electricimp.com/K5cBLKohWns7?heater=1\nhttps://agent.electricimp.com/K5cBLKohWns7?light=0\nhttps://agent.electricimp.com/K5cBLKohWns7?turnon=2";
//local responseString =""12.5\n24.8\n1011.4\n55\n81762\n";"


 
function requestHandler(request, response) {
  try {
    // check if the user sent led as a query parameter
    if ("temp" in request.query) {
      
      
        local temp = request.query.temp.tointeger();
 
        device.send("temp", temp); 
        server.log("Temp set to be " + temp);
      
    }
    else if("heater" in request.query){
    
        local heaterState = request.query.heater.tointeger();
        device.send("heaterState", heaterState);
        server.log("Heater state = "+ heaterState);
    }
    else if("light" in request.query){
        local lightState = request.query.light.tointeger();
        device.send("lightState", lightState);
        server.log("Light state = " + lightState);
    }
    else if("turnon" in request.query)
    {
        local turnon = request.query.turnon.tointeger();
        device.send("turnon", turnon)
        server.log("Turn on state = " + turnon)
    }
    else if("turnoff" in request.query)
    {
        local turnoff = request.query.turnoff.tointeger();
        device.send("turnoff", turnoff);
        server.log("Turn off state = " + turnoff);
    }
    else if("mode" in request.query)
    {
        local mode = request.query.mode.tointeger();
        device.send("mode", mode);
        server.log("mode=" + mode);
    }
    else if("page" in request.query)
    {
        local page = request.query.page.tointeger();
        device.send("page", page);
        server.log("page=" + page);
    }
    else if("grab" in request.query)
    {
        local grab = request.query.grab.tointeger();
        device.send("grab", grab);
        server.log("grab=" + grab);
    }
    
    // send a response back saying everything was OK.
    response.send(200, responseString);
    
  } catch (ex) {
    response.send(500, "Internal Server Error: " + ex);
  }
}

function uploadData(data)
{
    responseString = "";
    responseString = data;
    //server.log(responseString);
    
}

device.on("data", uploadData);
 
// register the HTTP handler
http.onrequest(requestHandler);