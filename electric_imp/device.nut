imp.enableblinkup(true);

tempString<-"";
presString<-"";
humString<-"";

local state = 0;
local pres = 0;
local hum = 0;
local tem = 0;

data <- "";
input_string <- "";
function setTemp(temp) {

  server.log("MOM<IMP:TEMP="+temp.tostring());
  hardware.uart57.write("MOM<IMP:TEMP="+temp.tostring());
  hardware.uart57.write(";\n")
}

function setHeater(heaterState)
{
  server.log("MOM<IMP:HEAT="+heaterState.tostring());
  hardware.uart57.write("MOM<IMP:HEAT="+heaterState.tostring());
  hardware.uart57.write(";\n")
}

function setLight(lightState)
{
  server.log("MOM<IMP:LIGH="+lightState.tostring());
  hardware.uart57.write("MOM<IMP:LIGH="+lightState.tostring());
  hardware.uart57.write(";\n")
}

function turnOn(turnon)
{
    server.log("MOM<IMP:TONN="+turnon.tostring());
  hardware.uart57.write("MOM<IMP:TONN="+turnon.tostring());
  hardware.uart57.write(";\n")
}

function turnOff(turnoff)
{
    server.log("MOM<IMP:TOFF="+turnoff.tostring());
  hardware.uart57.write("MOM<IMP:TOFF="+turnoff.tostring());
  hardware.uart57.write(";\n")
}

function Mode(mode)
{
    server.log("MOM<IMP:MODE="+mode.tostring());
  hardware.uart57.write("MOM<IMP:MODE="+mode.tostring());
  hardware.uart57.write(";\n")
}
function Page(page)
{
    server.log("MOM<IMP:PAGE="+page.tostring());
  hardware.uart57.write("MOM<IMP:PAGE="+page.tostring());
  hardware.uart57.write(";\n")
}
function Grab(grab)
{
    server.log("MOM<IMP:GRAB="+grab.tostring());
  hardware.uart57.write("MOM<IMP:GRAB="+grab.tostring());
  hardware.uart57.write(";\n")
}
function readback()
{

    
	local byte = hardware.uart57.read()
    
	// Ignore initial input
    
	if (byte == -1) return
	
	if(byte == 'T') state = 1;
	else if(byte == 'P') state = 2;
	else if(byte == 'H') state = 3;
    
	else if (byte == '\n')
	{
        if(state == 1)
        {
            tem = input_string.tofloat();
            //server.log("temp " + tem.tostring());
           // agent.send("temp", hardware.millis())
        }
        if(state == 2)
        {
            pres = input_string.tofloat()/100;
            //server.log("pres " + pres.tostring());
            //agent.send("pres", hardware.millis())
        }
        if(state == 3)
        {
            hum = input_string.tofloat();
            //server.log("hum " + hum.tostring());
            //agent.send("hum", hardware.millis())
        }
		//server.log("Sent string: " + input_string)
		data = tem.tostring()+"\n"+"25.8"+"\n"+pres.tostring()+"\n"+hum.tostring()+"\n"+"87453"+"\n"+"84\n" + "1982\n" + "";
		agent.send("data", data);
   
		input_string = "";
	}
	else
	{
		// Add the input character to the buffer
        
		input_string = input_string + chr(byte)
	}
}

function chr(asciiValue)
{
	// Convert passed integer value Ascii code into a character string
    
	if (asciiValue < 32) return ""
    	return format("%c", asciiValue)
}

 
hardware.uart57.configure(9600, 8, PARITY_NONE, 1, NO_CTSRTS,readback);

// register a handler for "led" messages from the agent

agent.on("temp", setTemp);
agent.on("heaterState", setHeater);
agent.on("lightState", setLight);
agent.on("turnon", turnOn);
agent.on("turnoff", turnOff);
agent.on("mode", Mode)
agent.on("page", Page)
agent.on("grab", Grab)