#include <ZumoMotors.h>
#define Z_THRES 10000
#define THRES 8000
#define P_THRES 200
char From[] = "GLV";
char To[] = "RVR";
int AX = 0;
int AY = 0;
int AZ = 0;
int GX = 0;
int GY = 0;
int GZ = 0;
int pr1 = 0;
int pr2 = 0;
int pr3 = 0;
int pr4 = 0;
int pr5 = 0;
int pr6 = 0;

#define LED_PIN 13
ZumoMotors motors;

void setup() {
  Serial.begin(9600);
  Serial1.begin(9600);
  pinMode(LED_PIN, OUTPUT);
  motors.flipLeftMotor(true);
  motors.flipRightMotor(true);

}

void loop() {
  if (Serial.available() > 0 || Serial1.available() > 0)
  {

    char message[50];
    memset(message, 0, 50);
    int a = 0;
    delay(100);
    while (Serial.available() > 0 || Serial1.available() > 0)
    {
      int c = 0;
      if(Serial.available() > 0) c = Serial.read();
      else if(Serial1.available() > 0) c = Serial1.read();
      if (c != ';')
      {
        message[a++] = c;

      }
      else
      {
        Serial.println(message);
          sscanf(message, "%3s<%3s:%d,%d,%d,%d,%d,%d,%d,%d,%d", From, To, &AX, &AY, &AZ, &pr1, &pr2, &pr3, &pr4, &pr5, &pr6);
          // RVR<GLV:0,-8000, 11000, 0, 0, 0, 0, 0, 0, 0, 0, 0;
          // RVR<GLV:0,-8000, 11000, 0, 0, 0, 160, 160, 0, 0, 0, 0;
          // RVR<GLV:0,-8000, 11000, 0, 0, 0, 200, 200, 0, 0, 0, 0;
          Serial.println(From);
          Serial.println(To);
          Serial.println(AX);
          Serial.println(AY);
          Serial.println(AZ);
          Serial.println(pr1);
          Serial.println(pr2);
          Serial.println(pr3);
          Serial.println(pr4);
          Serial.println(pr5);
          Serial.println(pr6);
          
          if (pr1 <= P_THRES && pr2 <= P_THRES)
          {
            if (AZ > Z_THRES && AY < -THRES) // FORWARD
            {
              motors.setLeftSpeed((P_THRES - pr4) + (P_THRES - pr4));
              motors.setRightSpeed((P_THRES - pr4) + (P_THRES - pr4));
            }
            else if (AZ > Z_THRES && AY > THRES) // BACKWARD
            {
              motors.setLeftSpeed(-(P_THRES - pr4) - (P_THRES - pr4));
              motors.setRightSpeed(-(P_THRES - pr4) - (P_THRES - pr4));
            }
            else if (AZ > Z_THRES && AX < -THRES) // RIGHT
            {
              motors.setLeftSpeed((P_THRES - pr4) + (P_THRES - pr4)/10);
              motors.setRightSpeed((-(P_THRES - pr4) - (P_THRES - pr4))/10);
            }
            else if (AZ > Z_THRES && AX > THRES) // LEFT
            {
              motors.setLeftSpeed((-(P_THRES - pr4) - (P_THRES - pr4))/10);
              motors.setRightSpeed((P_THRES - pr4) + (P_THRES - pr4)/10);
            }
            else
            {
              motors.setLeftSpeed(0);
              motors.setRightSpeed(0);
            }
            Serial.flush();
            Serial.end();
            Serial.begin(9600);
          }
          else
          {
            motors.setLeftSpeed(0);
            motors.setRightSpeed(0);
          }
      }

    }
  }
  //free(message);

}




