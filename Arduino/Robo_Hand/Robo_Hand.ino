#include <Servo.h>
#define STOP 0
#define SLOW_M 1
#define FAST_M 2
#define SERIAL_CONTROL 3



int X1 = 90;
int Y1 = 90;
int X2 = 90;
int Y2 = 90;
int grabVal = 0;
int macroSet = 0;

Servo ShowX;
Servo ShowY;
Servo Fingers;
Servo WristX;
Servo WristY;


void setup() {
  Fingers.attach(11);
  WristX.attach(10);
  WristY.attach(9);
  ShowY.attach(8);
  ShowX.attach(7);
  Serial.begin(9600);
  
  
  WristX.write(X2);
  delay(1000);
  WristY.write(Y2);
  delay(1000);
  
  delay(1000);
  ShowY.write(Y2);
  delay(1000);
  
 // while(1);

}

void loop() {
  
}

void macroControl(int macroToUse)
{
  if(Serial.available() > 0)
  {
    while(Serial.available() > 0)
    {
    char c = Serial.read();
    if(c == '0') macroSet = STOP;
    else if(c == '1') macroSet = SLOW_M;
    else if(c == '2') macroSet = FAST_M;
    else if(c == '3') macroSet = SERIAL_CONTROL;
    else;
    }
    
  }
  switch(macroToUse)
  {
    case STOP: break;
    case SLOW_M: slowMacro(); break;
    case FAST_M: fastMacro(); break;
    case SERIAL_CONTROL: break;
  }
}

void slowMacro()
{
  for(int i = X1; i < 90; i++)
  {
    ShowX.write(i);
    delay(10);
  }
  
}

void fastMacro()
{
  for(int i = X1; i < 90; i++)
  {
    ShowX.write(i);
    delay(5);
  }
}
void joysticControl()
{
  if (analogRead(A2) != 0)
  {
    if (analogRead(A0) > 600)
    {
      Y2 += ((analogRead(A0) - 600) / 400);
            if (Y2 > 180) Y2 = 180;
    }
    else if (analogRead(A0) < 400)
    {
      Y2 -= ((400 - analogRead(A0)) / 400);
            if (Y2 < 0) Y2 = 0;
    }
    if (analogRead(A1) > 600)
    {
      X2 += ((analogRead(A1) - 600) / 400);
            if (X2 > 180) X2 = 180;
    }
    else if (analogRead(A1) < 400)
    {
      X2 -= ((400 - analogRead(A1)) / 400);
            if (X2 < 0) X2 = 0;
    }
  }

  else if (analogRead(A2) == 0)
  {
    if (analogRead(A0) > 600)
    {
      Y1 += ((analogRead(A0) - 600) / 400);
            if (Y1 > 180) Y1 = 180;
    }
    else if (analogRead(A0) < 400)
    {
      Y1 -= ((400 - analogRead(A0)) / 400);
            if (Y1 < 0) Y1 = 0;
    }
    if (analogRead(A1) > 600)
    {
      X1 += ((analogRead(A1) - 600) / 400);
            if (X1 > 180) X1 = 180;
    }
    else if (analogRead(A1) < 400)
    {
      X1 -= ((400 - analogRead(A1)) / 400);
            if (X1 < 0) X1 = 0;
    }
  }
  delay(10);
  if(Serial.available() > 0)
  {
    String valueToRead;
    while(Serial.available() > 0)
    {
    char c = Serial.read();
    if(c != '\n')
    valueToRead += c;
    else 
    {
      grabVal = valueToRead.toInt();
    }
    }
    
  }
  Fingers.write(grabVal);
  WristX.write(X2);
  WristY.write(Y2);
  ShowY.write(Y1);
  ShowX.write(X1);
  char string[40];
  memset(string, 0, 40);
  sprintf(string, "%d,%d,%d,%d,%d\n", X1,Y1,X2,Y2,grabVal);
  Serial.print(string);
}
