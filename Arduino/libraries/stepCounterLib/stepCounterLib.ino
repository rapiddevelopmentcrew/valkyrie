#include <math.h>;

double userHeight;

double genderMultiplayer = 0.414; // average value
double genderMultiplayerMale = 0.415; // male value
double genderMultiplayerFimale = 0.413; // female value

int stepType;

int stepCounter;

int   Limit = 8; // SI units (m/s^2)
long	Timespan = 150; // ms.
long	LastTimeDiff;
long 	TimeThreshold = 300; // ms.

float   mYOffset = 240;

long	LastTimeStepCounted;

double   LastDirection = 0;
double   LastExtremes[6][6];
double   LastDiff = 0;
int     LastMatch = -1;

int   passX;
int   currentX;
int   passY;
int   currentY;
int   passZ;
int   currentZ;

float     passAxisAverage; // LastValueAvarage in java code
double   currentAxisAverage; //LastValueAvarage in java code
double   passAverageDirection;
// this function detect if there is next step to be teken in count. 
// Return can be:
// -> 0 for no step detected
// -> 1 for walk step detected
// -> 2 for run step detected
// -> 3 for stair up step detected 
// -> 4 for stair down step detected

int detectSteps(int X, int Y, int Z){

int passX;
int passY;
int passZ;

  
double XDiff = passX - X;

int XDirection = (XDiff > 0 ? 1 : 
                    (XDiff < 0 ? -1 : 0));

double YDiff = passY - Y;

int YDirection = (YDiff > 0 ? 1 :
                    (YDiff < 0 ? -1 : 0));

double ZDiff = passZ - Z;

int ZDirection = (ZDiff > 0 ? 1 : 
                    (ZDiff < 0 ? -1 : 0));
  
currentAxisAverage = (X + Y + Z)/3;

float currentAverageDirection = (currentAxisAverage > passAxisAverage ? 1 : 
                    (currentAxisAverage < passAxisAverage ? -1 : 0));

if (currentAverageDirection == - passAverageDirection) {
                    // Direction changed
                    int extType = (currentAverageDirection > 0 ? 0 : 1); // minumum or maximum?
                    LastExtremes[extType][0] = passAxisAverage;
                    float diff = fabs(LastExtremes[extType][0] - LastExtremes[1 - extType][0]); // what is this exactly !!!
                    
                    if (diff > Limit) { // is that decimal ... what is the pressition here
                    	long currentTime = millis();
                    	long timeDiff = currentTime - LastTimeStepCounted;
                    	
                        boolean isAlmostAsLargeAsPrevious = diff > (LastDiff*2L/3L);
                        boolean isPreviousLargeEnough = LastDiff > (diff/3L);
                        boolean isNotContra = (LastMatch != 1 - extType); // !!!
                        boolean isTimeBetweenStepsLargeEnough = timeDiff > Timespan;
                        boolean isTimeDiffAlmostAsLargeAsPrevious = 
                        	timeDiff >= LastTimeDiff - TimeThreshold 
                        	&& timeDiff <= LastTimeDiff + TimeThreshold;
                        
                        if (isAlmostAsLargeAsPrevious && isPreviousLargeEnough && isNotContra
                        		 && isTimeBetweenStepsLargeEnough && isTimeDiffAlmostAsLargeAsPrevious) {                          
                        	int stepType = calculateStepType(diff, timeDiff);
                        	
                        	stepCounter++;
                        	
                                
                        
                       // 	SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager(mContext);
                        	
                        	int steps;
                                int lastHourWalk;
                                int lastHourRun;
                                int lastHourStairsUp;
                                int lastHourStairsDown;
                                Serial.begin(9600);
                        	switch (stepType) {
							case 1:
								lastHourWalk += 1;
                                                                Serial.print("lastHourWalk = ");
                                                                Serial.println(lastHourWalk);
								break;
							case 2:
								lastHourRun += 1;
                                                                Serial.print("lastHourWalk = ");
                                                                Serial.println(lastHourRun);
                                                                break;
							case 3:
								lastHourStairsUp += 1;
								Serial.print("lastHourWalk = ");
                                                                Serial.println(lastHourStairsUp);
                                                                break;
							case 4:
								lastHourStairsDown += 1;
								Serial.print("lastHourWalk = ");
                                                                Serial.println(lastHourStairsDown);
                                                                break;
							}
                        	
      //                  	sendBroadcast(mContext, Constants.STEPCOUNTER_STEPS_DATA_MESSAGE, null);
                        	
      //                  	Log.w("Step ------->",""+stepCounter+" ["+stepType+"]");
                        	
      //                  	saveStep(currentTime, stepType);
      //                  	commitToDatabaseIfNeeded(currentTime);
                        	
                            LastMatch = extType;
                            LastTimeStepCounted = currentTime;
                        }
                        else {
                            LastMatch = -1;
                        }
                        LastTimeDiff = timeDiff;
                    }
                    LastDiff = diff;
                }


passAverageDirection = currentAverageDirection;
passAxisAverage = currentAxisAverage;
}

int calculateStepType (float distanceDiff, long timeDiff) {
		if (distanceDiff > Limit * 5) return 3; // stairs up
		if (timeDiff < Timespan * 2) return 2; // run
		return 1; // walk
	}



