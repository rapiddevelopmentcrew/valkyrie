// Light to Frequency test sketch

#define TSL_FREQ_PIN 2 // output use digital pin2 for interrupt
#define TSL_S0	     5
#define TSL_S1	     6
#define TSL_S2	     7
#define TSL_S3       8

// 1000ms = 1s
#define READ_TM 50

int multiplier = 2; // orig 100

unsigned long pulse_cnt = 0;

 // two variables used to track time
unsigned long cur_tm = millis();
unsigned long pre_tm = cur_tm;

 // we'll need to access the amount
 // of time passed
unsigned int tm_diff = 0;

unsigned long freq;

//************* SMOOTHING ***************//

const int numReadings = 8;

unsigned long readings[numReadings];      // the readings from the analog input
int index = 0;                  // the index of the current reading
unsigned long total = 0;                  // the running total
unsigned long average = 0;                // the average

///////////// SMOOTHING ////////////////


boolean beatDetect = false; 
int detectMargin = 26;

unsigned long minAverage = 100000;
unsigned long maxAverage = 0;

unsigned long minFreq = 100000;
unsigned long maxFreq = 0;


void setup() {
  
  Serial.begin(57600);

  // attach interrupt to pin2, send output pin of TSL230R to arduino 2
  // call handler on each rising pulse

 attachInterrupt(0, add_pulse, RISING);

 pinMode(TSL_FREQ_PIN, INPUT);
 pinMode(TSL_S0, OUTPUT);
 pinMode(TSL_S1, OUTPUT);
 pinMode(TSL_S2, OUTPUT);
 pinMode(TSL_S3, OUTPUT);

 digitalWrite(TSL_S0, LOW);
 digitalWrite(TSL_S1, HIGH);
 digitalWrite(TSL_S2, HIGH);
 digitalWrite(TSL_S3, LOW);
 
 ////////////// SMOOTHING ////////////////
 
 for (int thisReading = 0; thisReading < numReadings; thisReading++)
    readings[thisReading] = 0; 
 
 ///----////// SMOOTHING ////////////////
 
}

void loop() {
  // check the value of the light sensor every READ_TM ms

    // calculate how much time has passed

 pre_tm   = cur_tm;
 cur_tm   = millis();

 if( cur_tm > pre_tm ) {
	tm_diff += cur_tm - pre_tm;
 }
 else if( cur_tm < pre_tm ) {
           // handle overflow and rollover (Arduino 011)
	tm_diff += ( cur_tm + ( 34359737 - pre_tm ));
 } 

   // if enough time has passed to do a new reading...

 if( tm_diff >= READ_TM ) {

       // re-set the ms counter
   tm_diff = 0;

      // get our current frequency reading
   unsigned long frequency = get_tsl_freq();

 }
 

 

 
}

void add_pulse() {

  // increase pulse count
 pulse_cnt++;
 return;
}

unsigned long get_tsl_freq() {
    // copy pulse counter and multiply.
    // the multiplication is necessary for the current
    // frequency scaling level.  Please see the
    // OUTPUT SCALING section below for more info

  freq = pulse_cnt * multiplier;
  
  // increase resolution
//  freq = freq*4 - 200;

   // re-set pulse counter
  pulse_cnt = 0;
  
   ////////////// SMOOTHING ////////////////
 
 total -= readings[index];           
  readings[index] = freq; 
  total += readings[index];       
  index += 1;                    

  if (index >= numReadings)              
    index = 0;                           

  average = total / numReadings;
    
  ////////////// SMOOTHING ////////////////
  
  //************BEAT DETECT*************//
  
  
  if ((freq - (freq/170) > average) && beatDetect == false){
    beatDetect = true;
  }
  
  if ((freq + (freq/170) < average) && beatDetect == true){
    beatDetect = false;
  }
  
  ///////////BEAT DETECT/////////////////////
  
 Serial.print(beatDetect);
 Serial.print(",");
 Serial.print(average);
 Serial.print(",");
 Serial.println(freq); 

  return(freq);
}

