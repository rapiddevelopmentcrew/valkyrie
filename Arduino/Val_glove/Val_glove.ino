// I2C device class (I2Cdev) demonstration Arduino sketch for MPU6050 class
// 10/7/2011 by Jeff Rowberg <jeff@rowberg.net>
// Updates should (hopefully) always be available at https://github.com/jrowberg/i2cdevlib
//

#include "I2Cdev.h"
#include "MPU6050.h"


#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    #include "Wire.h"
#endif

// class default I2C address is 0x68
// specific I2C addresses may be passed as a parameter here
// AD0 low = 0x68 (default for InvenSense evaluation board)
// AD0 high = 0x69
MPU6050 accelgyro;
//MPU6050 accelgyro(0x69); // <-- use for AD0 high

int ax, ay, az;
int gx, gy, gz;


#define LED_PIN 13
bool blinkState = false;

void setup() {
    // join I2C bus (I2Cdev library doesn't do this automatically)
    #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
        Wire.begin();
    #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
        Fastwire::setup(400, true);
    #endif

    Serial.begin(9600);

    Serial.println("Initializing I2C devices...");
    accelgyro.initialize();

    Serial.println("Testing device connections...");
    Serial.println(accelgyro.testConnection() ? "MPU6050 connection successful" : "MPU6050 connection failed");

  

    // configure Arduino LED for
}

void loop() {
    // read raw accel/gyro measurements from device
    
    
              
        accelgyro.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);
        
        delay(100);
        char resultString[50];
        memset(resultString, 0, 50);
        int pr1 = analogRead(A0);
        int pr2 = analogRead(A1);
        int pr3 = analogRead(A2);
        int pr4 = analogRead(A3);
        int pr5 = analogRead(A7);
        int hr = analogRead(A6);
        char From[4] = "GLO";
        char To[4] = "MOT";
        sprintf(resultString, "%s<%s:%d,%d,%d,%d,%d,%d,%d,%d,%d;\n", From, To, ax,ay,az , pr1, pr2, pr3, pr4, pr5, hr);
        for(int i = 0; i < strlen(resultString); i++)
        {
          Serial.print(resultString[i]);
          //delay(10);
        }
       delay(200);
        
        
        

   
}
