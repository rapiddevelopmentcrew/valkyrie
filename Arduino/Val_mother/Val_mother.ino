#include <RCSwitch.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <idDHT11.h>
#include <Servo.h>



#define OFF 0
#define GESTURE 1
#define GRABBER 2
#define RAVEN 3

#define STOP 0
#define SLOW_M 1
#define FAST_M 2
#define CONTROL 3

#define Z_THRES 10000
#define THRES 8000
#define P_THRES 200


int X1 = 90;
int Y1 = 90;
int X2 = 90;
int Y2 = 90;
int grabVal = 0;
int grabberMode = OFF;

unsigned long mill[8];
int i = 0;
int switches[8] = {53, 51, 49, 47, 45, 43, 41, 39};
int desTemp = 25;
bool heaterState = 0;
bool lightState = 0;
bool switchesState[8] = {0, 0, 0, 0, 0, 0, 0, 0};

int mode = OFF;
int pr1 = 0;
int pr2 = 0;
int pr3 = 0;
int pr4 = 0;
int pr5 = 0;
int hr = 0;
int x = 0;
int y = 0;
int z = 0;

int idDHT11pin = 3; //Digital pin for comunications
int idDHT11intNumber = 3; //interrupt number (must be the one that use the previus defined pin (see table above)
void dht11_wrapper(); // must be declared before the lib initialization


double temperature = 0;
double humidity = 0;
double pressure = 985.12;
double luminocity = 87654;
int servosInitialized = false;

idDHT11 DHT11(idDHT11pin, idDHT11intNumber, dht11_wrapper);
RCSwitch mySwitch = RCSwitch();
LiquidCrystal_I2C lcd(0x27, 20, 4); // set the LCD address to 0x27 for a 20 chars and 4 line display

Servo ShowX;
Servo ShowY;
Servo Fingers;
Servo WristX;
Servo WristY;


void setup() {
  Serial1.begin(9600);
  Serial2.begin(9600);
  Serial3.begin(9600);
  SerialUSB.begin(9600);
  Serial.begin(9600);
  Keyboard.begin();
  lcd.init();
  lcd.backlight();
  for (int a = 0; a < 8; a++) pinMode(switches[a], OUTPUT);


  memset(mill, 0, 8);
  mySwitch.enableTransmit(2);
}

void loop() {
  imp_communication();
  xbee_communication();
  BLE_communication();


  if (millis() - mill[0] > 10000)
  {
    getHumAndTemp();
    SerialUSB.println("ALIVE");
    updateScreen(i++ % 3);
    updateImp();
    mill[0] = millis();
  }

}

void grabberActions()
{
  if (!servosInitialized)
  {
    Fingers.attach(11);
    WristX.attach(10);
    WristY.attach(9);
    ShowY.attach(8);
    ShowX.attach(7);
    servosInitialized = true;
  }
}

void grabberDettach()
{

}

void xbee_communication()
{
  if (Serial2.available() > 0)
  {
    delay(100);
    char message[80];
    memset(message, 0 , 80);
    int e = 0;
    char From[4];
    char To[4];
    SerialUSB.println("ONE");
    while (Serial2.available() > 0)
    {
      
      int c = Serial2.read();
      SerialUSB.print((int)c);
      if((c < ' ' || c > 'z')) 
      {
        Serial2.flush();
        break;
      }
      else if (c != ';') message[e++] = c;      
      else
      {
        //SerialUSB.println(message);
        sscanf(message, "%3s<%3s:%d,%d,%d,%d,%d,%d,%d,%d,%d", To, From, &x, &y, &z, &pr1, &pr2, &pr3, &pr4, &pr5, &hr);

        if (mode == RAVEN)
        {
          Serial2.print(message);
          Serial2.println(';');
        }
      }
    }
    Serial2.flush();
  }
}




void imp_communication()
{
  if (Serial3.available() > 0)
  {
    delay(100);
    char message[20];
    memset(message, 0, 20);
    char From[4];
    char To[4];
    char command[5];
    int Value = 0;
    int i = 0;
    while (Serial3.available() > 0)
    {
      int c = Serial3.read();
      if (c != ';') message[i++] = c;
      else
      {
        SerialUSB.println(message);
        sscanf(message, "%3s<%3s:%4s=%d", To, From, command, &Value);
        SerialUSB.println(To);
        SerialUSB.println(From);
        SerialUSB.println(command);
        SerialUSB.println(Value);
        if (!strcmp(command, "LIGH"))
        {
          lightState = Value;
          if (Value) mySwitch.send(1397845 , 24);
          else mySwitch.send(1397844, 24);
        }
        else if (!strcmp(command, "TEMP")) 
        {
          desTemp = Value;
          if(desTemp > temperature)
          {
            mySwitch.send(5510485, 24);
          }
          else if(desTemp < temperature)
          {
            mySwitch.send(5510484, 24);
          }
        }
        else if (!strcmp(command, "TONN"))
        {
          digitalWrite(switches[Value], LOW);
          switchesState[Value] = 1;
           if(Value == 1)
           {
             mySwitch.send(5633493, 24);
             delay(10);
             mySwitch.send(5571861, 24);
             delay(10);
             mySwitch.send(5571861, 24);
           }
           else if(Value == 2) 
           {
             mySwitch.send(5633493, 24);
             delay(10);
             mySwitch.send(5588245, 24);
             delay(10);
             mySwitch.send(5588245, 24);
             delay(10);
           }
           else if(Value == 3) 
           {
             mySwitch.send(5633493, 24);
             delay(10);
             mySwitch.send(5575957, 24);
             delay(10);
             mySwitch.send(5575957, 24);
             delay(10);
           }
           
        }
        else if (!strcmp(command, "TOFF"))
        {
          digitalWrite(switches[Value], HIGH);
          switchesState[Value] = 0;
          SerialUSB.println(Value);
          if(Value == 1)
          {
            mySwitch.send(5633493, 24);
            delay(10);
            mySwitch.send(5571860, 24);
            delay(10);
            mySwitch.send(5571860, 24);
          }
            
          else if(Value == 2)
         {
           mySwitch.send(5633493, 24);
           delay(10);
           mySwitch.send(5588244, 24);
           delay(10);
           mySwitch.send(5588244, 24);
         }
         
          else if(Value == 3) mySwitch.send(5575956, 24);
        }
        else if (!strcmp(command, "HEAT"))
        {
          heaterState = Value;
          if (Value) mySwitch.send(5510485, 24);
          else mySwitch.send(5510484, 24);
        }
        else if (!strcmp(command, "MODE")) mode = Value;
        else if (!strcmp(command, "PAGE"))
        {
          if (Value) Keyboard.print((char)0xD7);
          else Keyboard.print((char)0xD8);
        }
        else if (!strcmp(command, "GRAB"))
        {
            grabberMode = Value;
            Serial.write(20);
            Serial.print(message);
            Serial.println(';');
        }
      }
    }
  }
}

void BLE_communication()
{
  if (Serial1.available() > 0)
  {
    int c = Serial1.read();
    if (c == 'n')Keyboard.print((char)0xD7); // Right
    else if (c == 'p') Keyboard.print((char)0xD8); // LEFt %
  }
}

void getHumAndTemp()
{
  int result = DHT11.acquireAndWait();
  temperature = DHT11.getCelsius();
  humidity = DHT11.getHumidity();
}

void updateScreen(int lcdState)
{
  //lcd.clear();
  switch (lcdState)
  {
    case 0:
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Temperature:");
      lcd.print(temperature);
      lcd.print(" C");
      lcd.setCursor(0, 1);
      lcd.print("Humidity : ");
      lcd.print(humidity, 1);
      lcd.print(" %RH");
      lcd.setCursor(0, 2);
      lcd.print("Pressure: ");
      lcd.print(pressure);
      lcd.setCursor(0, 3);
      lcd.print("Luminocity: ");
      lcd.print(luminocity);
      break;
    case 1:
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Temperature:");
      lcd.print(temperature);
      lcd.print(" C");
      lcd.setCursor(0, 1);
      lcd.print("Target Tmp: ");
      lcd.print(desTemp);
      lcd.print(".00 C");
      lcd.setCursor(0, 2);
      lcd.print("Heater state: ");
      if (heaterState) lcd.print("ON");
      else lcd.print("OFF");
      lcd.setCursor(0, 3);
      lcd.print("Light steate: ");
      if (lightState) lcd.print("ON"); else lcd.print("OFF");
      break;
    case 2:
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("SW1 SW2 SW3 SW4 SW5");
      lcd.setCursor(0, 1);
      for (int y = 0; y <= 4; y++)
      {
        if (switchesState[y])lcd.print("ON  ");
        else lcd.print("OFF ");
      }
      lcd.setCursor(0, 2);
      lcd.print("SW6 SW7 SW8 SW9 SW10");
      lcd.setCursor(0, 3);
      for (int y = 5; y <= 7; y++)
      {
        if (switchesState[y])lcd.print("ON  ");
        else lcd.print("OFF ");
      }
      lcd.print("OFF ");
      lcd.print("OFF ");
      break;

  }

}

void dht11_wrapper() {
  DHT11.isrCallback();
}

void updateImp()
{
  char dataT[40];
  memset(dataT, 0, 40);
  sprintf(dataT, "T%0.2f\nP%0.2f\nH%0.2f\n", temperature, pressure * 100, humidity);
  Serial3.print(dataT);
  SerialUSB.print(dataT);
}


